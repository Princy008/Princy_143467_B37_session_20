<?php

use App\Person;
use App\Student;



//this is autoload ,for dynamic
function __autoload($className)
{

     list($ns,$cn)=explode("\\",$className);               //classnmae is a local var
    require_once("../../src/Bitm/SEIP_143467/".$cn.".php");

}

$obj=new Person();

echo $obj->showPersonInfo();




// this is normal

/*
require_once("../../src/Bitm/SEIP_143467/Person.php");
require_once("../../src/Bitm/SEIP_143467/Student.php");

$obj=new Person();  //use App\Person  for  person info accessing

echo $obj->showPersonInfo();

$obj1=new Student();//use App\Student for  student property accessing

echo $obj1->showStudentInfo();
*/