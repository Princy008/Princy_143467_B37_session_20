<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 10/27/2016
 * Time: 8:00 PM
 */

namespace App;


class Student extends Person
{
    public $studentId="143467";

    public function showStudentInfo()
    {
        parent::showPersonInfo();
        echo $this->studentId."<br>";
    }

}