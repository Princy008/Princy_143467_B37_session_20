<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 10/27/2016
 * Time: 7:55 PM
 */

namespace App;

class Person
{
 public $name="Princy";
    public $gender="female";
    public $blood_group="B+";


    public function showPersonInfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}